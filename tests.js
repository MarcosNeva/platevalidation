test("CorrectOrWrongData", function (assert) {
   	assert.equal(isValidPlate("1234HJK"), true, "Test1");
	assert.equal(isValidPlate("1234QLL"), false, "Test2");
	assert.equal(isValidPlate("0417FMK"), true, "Test3");
	assert.equal(isValidPlate("1234ÑRT"), false, "Test4");
	assert.equal(isValidPlate("1000HDP"), true, "Test5");
	assert.equal(isValidPlate("1664ALO"), false, "Test6");
	assert.equal(isValidPlate("1675GXS"), true, "Test7");
	assert.equal(isValidPlate("1277EEE"), false, "Test8");
});


test("CorrectOrWrongOrder", function (assert) {
   	assert.equal(isValidPlate("1234HJK"), true, "Test1");
	assert.equal(isValidPlate("12Q34LL"), false, "Test2");
	assert.equal(isValidPlate("0417FMK"), true, "Test3");
	assert.equal(isValidPlate("12B34RT"), false, "Test4");
	assert.equal(isValidPlate("10V00HD"), false, "Test5");
	assert.equal(isValidPlate("16A64LO"), false, "Test6");
	assert.equal(isValidPlate("1675GXS"), true, "Test7");
	assert.equal(isValidPlate("1E77CE2"), false, "Test8");
});


test("MoreNumbers", function (assert) {
   	assert.equal(isValidPlate("12345JK"), false, "Test1");
	assert.equal(isValidPlate("12345LL"), false, "Test2");
	assert.equal(isValidPlate("04175MK"), false, "Test3");
	assert.equal(isValidPlate("12345RT"), false, "Test4");
	assert.equal(isValidPlate("10005DP"), false, "Test5");
	assert.equal(isValidPlate("16645LL"), false, "Test6");
	assert.equal(isValidPlate("1675GXS"), true, "Test7");
	assert.equal(isValidPlate("12777EE"), false, "Test8");
});


test("MoreLetters", function (assert) {
   	assert.equal(isValidPlate("123DHJK"), false, "Test1");
	assert.equal(isValidPlate("624AALL"), false, "Test2");
	assert.equal(isValidPlate("349FBPL"), false, "Test3");
	assert.equal(isValidPlate("123KALL"), false, "Test4");
	assert.equal(isValidPlate("0417FMK"), true, "Test5");
	assert.equal(isValidPlate("877XXPG"), false, "Test6");
	assert.equal(isValidPlate("453LQÑA"), false, "Test7");
	assert.equal(isValidPlate("123CCLL"), false, "Test8");
});


test("NotComplete", function (assert) {
   	assert.equal(isValidPlate("12HJK"), false, "Test1");
	assert.equal(isValidPlate("345LL"), false, "Test2");
	assert.equal(isValidPlate("1234JFH"), true, "Test3");
	assert.equal(isValidPlate("12L"), false, "Test4");
	assert.equal(isValidPlate("1234HLL"), true, "Test5");
	assert.equal(isValidPlate("1234"), false, "Test6");
	assert.equal(isValidPlate("1333MNB"), true, "Test7");
	assert.equal(isValidPlate("LL"), false, "Test8");
});


test("OverData", function (assert) {
   	assert.equal(isValidPlate("1333GYT"), true,"Test1");
	assert.equal(isValidPlate("12G345LLF"), false,"Test2");
	assert.equal(isValidPlate("12J534HJKB4"), false,"Test3");
	assert.equal(isValidPlate("126345LL"), false,"Test4");
	assert.equal(isValidPlate("123F664HJK"), false,"Test5");
	assert.equal(isValidPlate("123485LL"), false,"Test6");
	assert.equal(isValidPlate("4567JPL"), true,"Test7");
	assert.equal(isValidPlate("1002345LL"), false,"Test8");
});





