/*
 * Given the three sides of a triangle it calculates the type of the
 * triangle.
 */
function isValidPlate(plate) {
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  if(plate.match(re) != null) {

    gtag('event', 'click',{
      'event_category':'ValidityState',
      'event_label':'true'
    });
    return true;

   

  } else {

    gtag('event', 'click',{
      'event_category':'ValidityState',
      'event_label':'false'
    });
    return false;
  }
}